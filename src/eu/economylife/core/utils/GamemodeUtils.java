/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.utils;

import org.bukkit.GameMode;

public interface GamemodeUtils
{

	public static GameMode get(String value){
		
		if(value == null)
			return null;
		
		if(value.equalsIgnoreCase("0") || value.equalsIgnoreCase("survival") || value.toLowerCase().startsWith("survival".toLowerCase()))
			return GameMode.SURVIVAL;
		
		if(value.equalsIgnoreCase("1") || value.equalsIgnoreCase("creative") || value.toLowerCase().startsWith("creative".toLowerCase()))
			return GameMode.CREATIVE;
		
		if(value.equalsIgnoreCase("2") || value.equalsIgnoreCase("adventure") || value.toLowerCase().startsWith("adventure".toLowerCase()))
			return GameMode.ADVENTURE;
		
		if(value.equalsIgnoreCase("3") || value.equalsIgnoreCase("spectator") || value.toLowerCase().startsWith("spectator".toLowerCase()))
			return GameMode.SPECTATOR;
		
		return null;
	}
}
