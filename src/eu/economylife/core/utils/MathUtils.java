/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.utils;

import java.util.Random;

public interface MathUtils
{
	
	public static boolean isInteger(String s)
	{
		try
		{
			Integer.parseInt(s);
		} catch (NumberFormatException e)
		{
			return false;
		} catch (NullPointerException e)
		{
			return false;
		}

		return true;
	}
	
	public static Integer getInteger(String s)
	{
		try
		{
			return Integer.parseInt(s);
		} catch (NumberFormatException e)
		{
			return 0;
		} catch (NullPointerException e)
		{
			return 0;
		}
	}
	
	public static boolean isFloat(String s)
	{
		try
		{
			Float.parseFloat(s);
		} catch (NumberFormatException e)
		{
			return false;
		} catch (NullPointerException e)
		{
			return false;
		}

		return true;
	}
	
	public static float getFloat(String s)
	{
		try
		{
			return Float.parseFloat(s);
		} catch (NumberFormatException e)
		{
			return 0;
		} catch (NullPointerException e)
		{
			return 0;
		}
	}

	public static int random(int min, int max)
	{
		return new Random().nextInt((max - min) + 1) + min;
	}
}
