/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.permissions;

import org.bukkit.configuration.file.YamlConfiguration;

import eu.economylife.core.EconomyLife;
import eu.economylife.core.config.YamlConfigLoader;

public class PermissionsManager
{

	private YamlConfigLoader yamlConfigLoader;
	private YamlConfiguration config;

	public PermissionsManager()
	{
		yamlConfigLoader = new YamlConfigLoader("Permssions.yml", "Permissions.yml");
		config = yamlConfigLoader.getYml();
	}

	public static void addDefault(String path, int value)
	{
		PermissionsManager permissionsManager = EconomyLife.getPermissionsManager();

		if (!permissionsManager.getConfig().contains("config." + path))
		{
			permissionsManager.getConfig().set("config." + path, value);
			permissionsManager.getYamlConfigLoader().save();
		}
	}

	public static int getPermissionLevel(String path)
	{
		PermissionsManager permissionsManager = EconomyLife.getPermissionsManager();
		
		return (permissionsManager.getConfig().contains("config." + path))
				? permissionsManager.getConfig().getInt("config." + path) : -1;
	}

	public YamlConfigLoader getYamlConfigLoader()
	{
		return yamlConfigLoader;
	}
	
	public YamlConfiguration getConfig()
	{
		return config;
	}
}
