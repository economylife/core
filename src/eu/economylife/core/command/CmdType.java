/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command;

public enum CmdType
{
	SUCCESS,
	ERROR,
	SYNTAX_ERROR,
	NO_PERMISSION;
}
