/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;

import eu.economylife.core.EconomyLife;
import eu.economylife.core.chat.Chat;
import eu.economylife.core.chat.ChatArg;
import eu.economylife.core.chat.MessageType;
import eu.economylife.core.ecoplayer.EcoPlayer;
import eu.economylife.core.ecoplayer.EcoPlayerManager;
import eu.economylife.core.group.Group;
import eu.economylife.core.language.LanguageManager;

public class CommandManager implements Listener
{
	public HashMap<String, OnCommandListener> commands;
	public ArrayList<String> commandsWhitelist;

	public CommandManager()
	{
		EconomyLife economyLife = EconomyLife.getInstance();
		economyLife.getServer().getPluginManager().registerEvents(this, economyLife);

		commands = new HashMap<>();
		commandsWhitelist = new ArrayList<>();
		
		LanguageManager.addDefault("command.syntax", "Syntax %c");
		LanguageManager.addDefault("command.unknown", "The command %c does not exist!");
		LanguageManager.addDefault("command.nopermission", "You do not have permission to use this command!");
	}

	@EventHandler
	public void onConsoleCommand(ServerCommandEvent e)
	{
		String[] cmdSplit = e.getCommand().split(" ");

		if (isCommandWhitelisted(cmdSplit[0]))
			return;

		e.setCancelled(true);

		String cmd = "/" + cmdSplit[0];
		OnCommandListener listener = getOnCommandListener(cmd);

		if (listener == null)
		{
			System.out.println("The command " + cmdSplit[0] +" does not exist!");
			return;
		}

		String[] args = getArgs(e.getCommand());
		
		switch(listener.onConsoleCommand(cmd, args)) {
		case SUCCESS:
			break;
		case NO_PERMISSION:
			System.out.println("This command cannot be used by the console!");
			break;
		case SYNTAX_ERROR:
			System.out.println("Syntax " + listener.getSyntax());
			break;
		default:
			break;
		}
	}
	
	//TODO Auslagern
	@EventHandler
	public void onPlayerChat(AsyncPlayerChatEvent e)
	{
		
		e.setCancelled(true);
		
		EcoPlayer target = EcoPlayerManager.getEcoPlayer(e.getPlayer());
		Group group = target.getGroup();
		
		Bukkit.getServer().broadcastMessage("§7[" + group.getPrefix() + group.getShortName() + "§7] " + group.getPrefix() + target.getName() + "§f: " + e.getMessage());

	}
	
	@EventHandler
	public void onPlayerCommand(PlayerCommandPreprocessEvent e)
	{
		String[] cmdSplit = e.getMessage().split(" ");

		if (isCommandWhitelisted(cmdSplit[0]))
			return;

		e.setCancelled(true);

		Player player = e.getPlayer();
		String cmd = cmdSplit[0];
		OnCommandListener listener = getOnCommandListener(cmd);

		if (listener == null)
		{
			Chat.sendMessage(player, MessageType.ERROR, "command.unknown", new ChatArg("%c", cmd));
			return;
		}

		EcoPlayer ecoPlayer = EcoPlayerManager.getEcoPlayer(player);
		
		String[] args = getArgs(e.getMessage());

		switch(listener.onPlayerCommand(ecoPlayer, cmd, args)) {
		case SUCCESS:
			break;
		case NO_PERMISSION:
			Chat.sendMessage(player, MessageType.ERROR, "command.nopermission", new ChatArg("%c", listener.getSyntax()));
			break;
		case SYNTAX_ERROR:
			Chat.sendMessage(player, MessageType.NORMAL, "command.syntax", new ChatArg("%c", listener.getSyntax()));
			break;
		default:
			break;
		}
	}

	public void registerCommand(OnCommandListener listener, String... cmds)
	{
		for (String cmd : cmds)
			if (!commands.containsKey(cmd))
				commands.put(cmd, listener);
	}

	public OnCommandListener getOnCommandListener(String cmd)
	{
		if (!commands.containsKey(cmd))
			return null;

		return commands.get(cmd);
	}

	public void addCommandToWhitelist(String... cmds)
	{
		for (String cmd : cmds)
		{
			cmd = cmd.replace("/", "");
			if (!commandsWhitelist.contains(cmd))
				commandsWhitelist.add(cmd);
		}
	}

	public boolean isCommandWhitelisted(String cmd)
	{
		return commandsWhitelist.contains(cmd.replace("/", ""));
	}

	private String[] getArgs(String message)
	{

		String[] cmdSplit = message.split(" ");
		String[] args = new String[cmdSplit.length - 1];
		for (int i = 1; i < cmdSplit.length; i++)
			args[i - 1] = cmdSplit[i];
		return args;
	}
}
