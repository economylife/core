/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command.admin;

import org.bukkit.GameMode;

import eu.economylife.core.chat.Chat;
import eu.economylife.core.chat.ChatArg;
import eu.economylife.core.chat.MessageType;
import eu.economylife.core.command.CmdType;
import eu.economylife.core.command.OnCommandListener;
import eu.economylife.core.ecoplayer.EcoPlayer;
import eu.economylife.core.ecoplayer.EcoPlayerManager;
import eu.economylife.core.group.Group;
import eu.economylife.core.group.GroupManager;
import eu.economylife.core.language.LanguageManager;
import eu.economylife.core.permissions.PermissionsManager;
import eu.economylife.core.utils.GamemodeUtils;

public class GroupCommand extends OnCommandListener
{
	@Override
	public void onInit()
	{
		setSyntax("/group (player) (group)");
		setDescription("Set Group");

		LanguageManager.addDefault("command.group.sender.success", "You have changed the group from Player %p to %g!");
		LanguageManager.addDefault("command.group.target.success", "Your group has been changed to %g.");
		LanguageManager.addDefault("command.target.offline", "The Player %p is offline!");
		LanguageManager.addDefault("command.group.doesntexist", "The group %g doesnt exist!");

		PermissionsManager.addDefault("command.group", 75);
	}

	@Override
	public CmdType onConsoleCommand(String cmd, String[] args)
	{
		return CmdType.NO_PERMISSION;
	}

	@Override
	public CmdType onPlayerCommand(EcoPlayer ecoPlayer, String cmd, String[] args)
	{
		if (!ecoPlayer.hasPermissions("command.group"))
			return CmdType.NO_PERMISSION;

		if (args.length != 2)
			return CmdType.SYNTAX_ERROR;

		EcoPlayer target = EcoPlayerManager.getEcoPlayer(args[0]);
		if (target == null)
		{
			Chat.sendMessage(target, MessageType.ERROR, "command.target.offline",
					new ChatArg("%p", args[0]));
			return CmdType.ERROR;
		}

		Group group = GroupManager.getGroupByName(args[1]);
		if (group == null)
		{
			Chat.sendMessage(target, MessageType.ERROR, "command.group.doesntexist",
					new ChatArg("%g", args[args.length - 1]));
			return CmdType.ERROR;
		}

		target.setGroup(group);
		
		Chat.sendMessage(target, MessageType.NORMAL, "command.group.target.success",
				new ChatArg("%g", group.getName()));

		if (!target.getUUID().equals(ecoPlayer.getUUID()))
			Chat.sendMessage(ecoPlayer, MessageType.NORMAL, "command.group.sender.success",
					new ChatArg("%g", group.getName()), new ChatArg("%p", target.getName()));

		return CmdType.SUCCESS;
	}
}