/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command.admin;

import org.bukkit.GameMode;

import eu.economylife.core.chat.Chat;
import eu.economylife.core.chat.ChatArg;
import eu.economylife.core.chat.MessageType;
import eu.economylife.core.command.CmdType;
import eu.economylife.core.command.OnCommandListener;
import eu.economylife.core.ecoplayer.EcoPlayer;
import eu.economylife.core.ecoplayer.EcoPlayerManager;
import eu.economylife.core.group.Group;
import eu.economylife.core.group.GroupManager;
import eu.economylife.core.language.LanguageManager;
import eu.economylife.core.permissions.PermissionsManager;
import eu.economylife.core.utils.GamemodeUtils;
import eu.economylife.core.utils.MathUtils;

public class AddMoneyCommand extends OnCommandListener
{
	@Override
	public void onInit()
	{
		setSyntax("/addmoney (player) (amount)");
		setDescription("Add Money");

		LanguageManager.addDefault("command.addmoney.sender.success", "You gave the player %p %m, he has now %nm!");
		LanguageManager.addDefault("command.addmoney.target.success", "%m euro have been added to you by %p - now you have %om!");
		LanguageManager.addDefault("command.target.offline", "The Player %p is offline!");

		PermissionsManager.addDefault("command.addmoney", 75);
	}

	@Override
	public CmdType onConsoleCommand(String cmd, String[] args)
	{
		return CmdType.NO_PERMISSION;
	}

	@Override
	public CmdType onPlayerCommand(EcoPlayer ecoPlayer, String cmd, String[] args)
	{
		if (!ecoPlayer.hasPermissions("command.addmoney"))
			return CmdType.NO_PERMISSION;

		if (args.length != 2)
			return CmdType.SYNTAX_ERROR;

		EcoPlayer target = EcoPlayerManager.getEcoPlayer(args[0]);
		if (target == null)
		{
			Chat.sendMessage(target, MessageType.ERROR, "command.target.offline",
					new ChatArg("%p", args[0]));
			return CmdType.ERROR;
		}

		if (!MathUtils.isInteger(args[1]))
			return CmdType.SYNTAX_ERROR;
		
		int money = MathUtils.getInteger(args[1]);

		target.addMoney(money);
		
		Chat.sendMessage(target, MessageType.NORMAL, "command.addmoney.target.success",
				new ChatArg("%p", ecoPlayer.getName()), new ChatArg("%m", money + "€"), new ChatArg("%om", target.getMoney() + "€"));

		if (!target.getUUID().equals(ecoPlayer.getUUID()))
			Chat.sendMessage(ecoPlayer, MessageType.NORMAL, "command.addmoney.sender.success",
					new ChatArg("%p", target.getName()), new ChatArg("%m", money + "€"), new ChatArg("%nm", target.getMoney() + "€"));

		return CmdType.SUCCESS;
	}
}