/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command.admin;

import org.bukkit.configuration.file.YamlConfiguration;

import eu.economylife.core.chat.Chat;
import eu.economylife.core.chat.MessageType;
import eu.economylife.core.command.CmdType;
import eu.economylife.core.command.OnCommandListener;
import eu.economylife.core.config.YamlConfigLoader;
import eu.economylife.core.ecoplayer.EcoPlayer;
import eu.economylife.core.language.LanguageManager;
import eu.economylife.core.permissions.PermissionsManager;
import eu.economylife.core.utils.LocationUtils;

public class SetLobbyCommand extends OnCommandListener
{
	@Override
	public void onInit()
	{
		setSyntax("/setlobby");
		setDescription("Set Lobby");

		LanguageManager.addDefault("command.set.lobbysuccess", "You put the lobby on your position!");

		PermissionsManager.addDefault("command.setlobby", 75);
	}

	@Override
	public CmdType onConsoleCommand(String cmd, String[] args)
	{
		return CmdType.NO_PERMISSION;
	}

	@Override
	public CmdType onPlayerCommand(EcoPlayer ecoPlayer, String cmd, String[] args)
	{
		if (!ecoPlayer.hasPermissions("command.setlobby"))
			return CmdType.NO_PERMISSION;

		if (args.length != 0)
			return CmdType.SYNTAX_ERROR;

		YamlConfigLoader configLoader = new YamlConfigLoader("Config.yml");
		YamlConfiguration config = configLoader.getYml();
		config.set("config.player.location.lobby", LocationUtils.toString(ecoPlayer.getLocation()));
		configLoader.save();
		
		Chat.sendMessage(ecoPlayer, MessageType.NORMAL, "command.setlobby.success");
		
		return CmdType.SUCCESS;
	}
}