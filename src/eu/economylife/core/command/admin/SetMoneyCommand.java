/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command.admin;

import org.bukkit.GameMode;

import eu.economylife.core.chat.Chat;
import eu.economylife.core.chat.ChatArg;
import eu.economylife.core.chat.MessageType;
import eu.economylife.core.command.CmdType;
import eu.economylife.core.command.OnCommandListener;
import eu.economylife.core.ecoplayer.EcoPlayer;
import eu.economylife.core.ecoplayer.EcoPlayerManager;
import eu.economylife.core.group.Group;
import eu.economylife.core.group.GroupManager;
import eu.economylife.core.language.LanguageManager;
import eu.economylife.core.permissions.PermissionsManager;
import eu.economylife.core.utils.GamemodeUtils;
import eu.economylife.core.utils.MathUtils;

public class SetMoneyCommand extends OnCommandListener
{
	@Override
	public void onInit()
	{
		setSyntax("/setmoney (player) (amount)");
		setDescription("Set Money");

		LanguageManager.addDefault("command.setmoney.sender.success", "You bet the money from the player %p on %m!");
		LanguageManager.addDefault("command.setmoney.target.success", "Your money's been wagered on %m!");
		LanguageManager.addDefault("command.target.offline", "The Player %p is offline!");

		PermissionsManager.addDefault("command.setmoney", 75);
	}

	@Override
	public CmdType onConsoleCommand(String cmd, String[] args)
	{
		return CmdType.NO_PERMISSION;
	}

	@Override
	public CmdType onPlayerCommand(EcoPlayer ecoPlayer, String cmd, String[] args)
	{
		if (!ecoPlayer.hasPermissions("command.setmoney"))
			return CmdType.NO_PERMISSION;

		if (args.length != 2)
			return CmdType.SYNTAX_ERROR;

		EcoPlayer target = EcoPlayerManager.getEcoPlayer(args[0]);
		if (target == null)
		{
			Chat.sendMessage(target, MessageType.ERROR, "command.target.offline",
					new ChatArg("%p", args[0]));
			return CmdType.ERROR;
		}

		if (!MathUtils.isInteger(args[1]))
			return CmdType.SYNTAX_ERROR;
		
		
		int money = MathUtils.getInteger(args[1]);

		target.setMoney(money);
		
		Chat.sendMessage(target, MessageType.NORMAL, "command.setmoney.target.success",
				new ChatArg("%p", target.getName()), new ChatArg("%m", money + "€"));

		if (!target.getUUID().equals(ecoPlayer.getUUID()))
			Chat.sendMessage(ecoPlayer, MessageType.NORMAL, "command.setmoney.sender.success",
					new ChatArg("%m", money + "€"));

		return CmdType.SUCCESS;
	}
}