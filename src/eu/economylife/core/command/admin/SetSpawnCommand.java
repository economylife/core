/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command.admin;

import org.bukkit.configuration.file.YamlConfiguration;

import eu.economylife.core.chat.Chat;
import eu.economylife.core.chat.MessageType;
import eu.economylife.core.command.CmdType;
import eu.economylife.core.command.OnCommandListener;
import eu.economylife.core.config.YamlConfigLoader;
import eu.economylife.core.ecoplayer.EcoPlayer;
import eu.economylife.core.language.LanguageManager;
import eu.economylife.core.permissions.PermissionsManager;
import eu.economylife.core.utils.LocationUtils;

public class SetSpawnCommand extends OnCommandListener
{
	@Override
	public void onInit()
	{
		setSyntax("/setspawn");
		setDescription("Set Spawn");

		LanguageManager.addDefault("command.setspawn.success", "You put the spawn on your position!");

		PermissionsManager.addDefault("command.setspawn", 75);
	}

	@Override
	public CmdType onConsoleCommand(String cmd, String[] args)
	{
		return CmdType.NO_PERMISSION;
	}

	@Override
	public CmdType onPlayerCommand(EcoPlayer ecoPlayer, String cmd, String[] args)
	{
		if (!ecoPlayer.hasPermissions("command.setspawn"))
			return CmdType.NO_PERMISSION;

		if (args.length != 0)
			return CmdType.SYNTAX_ERROR;

		YamlConfigLoader configLoader = new YamlConfigLoader("Config.yml");
		YamlConfiguration config = configLoader.getYml();
		config.set("config.player.location.spawn", LocationUtils.toString(ecoPlayer.getLocation()));
		configLoader.save();
		
		Chat.sendMessage(ecoPlayer, MessageType.NORMAL, "command.setspawn.success");
		
		return CmdType.SUCCESS;
	}
}