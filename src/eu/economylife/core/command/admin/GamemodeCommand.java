/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command.admin;

import org.bukkit.GameMode;

import eu.economylife.core.chat.Chat;
import eu.economylife.core.chat.ChatArg;
import eu.economylife.core.chat.MessageType;
import eu.economylife.core.command.CmdType;
import eu.economylife.core.command.OnCommandListener;
import eu.economylife.core.ecoplayer.EcoPlayer;
import eu.economylife.core.ecoplayer.EcoPlayerManager;
import eu.economylife.core.language.LanguageManager;
import eu.economylife.core.permissions.PermissionsManager;
import eu.economylife.core.utils.GamemodeUtils;

public class GamemodeCommand extends OnCommandListener
{
	@Override
	public void onInit()
	{
		setSyntax("/gamemode <player> (gamemode)");
		setDescription("Set Gamemode");

		LanguageManager.addDefault("command.gamemode.sender.success",
				"You have changed the gamemode from Player %p to %g!");
		LanguageManager.addDefault("command.gamemode.target.success", "Your game mode has been changed to %g.");
		LanguageManager.addDefault("command.target.offline", "The Player %p is offline!");
		LanguageManager.addDefault("command.gamemode.doesntexist", "The Gamemode %g doesnt exist!");

		PermissionsManager.addDefault("command.gamemode", 75);
	}

	@Override
	public CmdType onConsoleCommand(String cmd, String[] args)
	{
		return CmdType.NO_PERMISSION;
	}

	@Override
	public CmdType onPlayerCommand(EcoPlayer ecoPlayer, String cmd, String[] args)
	{
		if (!ecoPlayer.hasPermissions("command.gamemode"))
			return CmdType.NO_PERMISSION;

		if (args.length != 1 && args.length != 2)
			return CmdType.SYNTAX_ERROR;

		EcoPlayer target = ecoPlayer;
		if (args.length == 2)
		{

			EcoPlayer temp = EcoPlayerManager.getEcoPlayer(args[0]);
			if (temp == null)
			{
				Chat.sendMessage(target, MessageType.ERROR, "command.target.offline",
						new ChatArg("%p", args[0]));
				return CmdType.ERROR;
			}

			target = temp;
		}

		GameMode gamemode = GamemodeUtils.get(args[args.length - 1]);
		if (gamemode == null)
		{
			Chat.sendMessage(target, MessageType.ERROR, "command.gamemode.doesntexist",
					new ChatArg("%g", args[args.length - 1]));
			return CmdType.ERROR;
		}

		target.getPlayer().setGameMode(gamemode);
		Chat.sendMessage(target, MessageType.NORMAL, "command.gamemode.target.success",
				new ChatArg("%g", gamemode.name()));

		if (!target.getUUID().equals(ecoPlayer.getUUID()))
			Chat.sendMessage(ecoPlayer, MessageType.NORMAL, "command.gamemode.sender.success",
					new ChatArg("%g", gamemode.name()), new ChatArg("%p", target.getName()));

		return CmdType.SUCCESS;
	}
}