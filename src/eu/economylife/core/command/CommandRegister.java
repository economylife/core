/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command;

import eu.economylife.core.EconomyLife;
import eu.economylife.core.command.admin.AddMoneyCommand;
import eu.economylife.core.command.admin.GamemodeCommand;
import eu.economylife.core.command.admin.GroupCommand;
import eu.economylife.core.command.admin.SetLobbyCommand;
import eu.economylife.core.command.admin.SetMoneyCommand;
import eu.economylife.core.command.admin.SetSpawnCommand;

public class CommandRegister
{

	public CommandRegister()
	{
		CommandManager cm = EconomyLife.getCommandManager();
		
		cm.addCommandToWhitelist("/reload");
		cm.addCommandToWhitelist("/op", "/deop");
		
		cm.registerCommand(new GamemodeCommand(), "/gamemode", "/gm");
		cm.registerCommand(new GroupCommand(), "/group");
		cm.registerCommand(new SetSpawnCommand(), "/setspawn");
		cm.registerCommand(new SetLobbyCommand(), "/setlobby");
		cm.registerCommand(new SetMoneyCommand(), "/setmoney");
		cm.registerCommand(new AddMoneyCommand(), "/addmoney");
	}
}
