/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.command;

import eu.economylife.core.ecoplayer.EcoPlayer;

public abstract class OnCommandListener
{
	
	private String syntax = "";
	private String description = "";
	
	public OnCommandListener(){
		
		init();
		onInit();
	}
	
	public void init(){
		
	}
	
	public String getSyntax()
	{
		return syntax;
	}

	public void setSyntax(String syntax)
	{
		this.syntax = syntax;
	}

	public String getDescription()
	{
		return description;
	}

	public void setDescription(String description)
	{
		this.description = description;
	}

	public abstract void onInit();
	public abstract CmdType onConsoleCommand(String cmd, String[] args);
	public abstract CmdType onPlayerCommand(EcoPlayer ecoPlayer, String cmd, String[] args);
}
