/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.language;

import org.bukkit.configuration.file.YamlConfiguration;

import eu.economylife.core.config.YamlConfigLoader;

public class Language
{
	private String code;
	
	private YamlConfigLoader configLoader;
	private YamlConfiguration config;
	
	public Language(String code){
		
		this.code = code;
		
		configLoader = new YamlConfigLoader("lang/" + code + ".yml");
		config = configLoader.getYml();
	}
	
	public void addDefault(String path, String value){
		if(!config.contains(code.toLowerCase() + "." + path))
		{
			config.set(code.toLowerCase() + "." + path, value);
			configLoader.save();
		}
	}
	
	public String getString(String path){
		return config.getString(code.toLowerCase() + "." + path);
	}
}
