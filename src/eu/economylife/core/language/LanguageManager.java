/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.language;

import java.io.File;
import java.util.HashMap;
import java.util.logging.Level;

import eu.economylife.core.EconomyLife;
import eu.economylife.core.config.YamlConfigLoader;
import eu.economylife.core.ecoplayer.EcoPlayer;

public class LanguageManager
{

	private HashMap<String, Language> languages;

	public LanguageManager()
	{

		languages = new HashMap<>();

		// Create Language Config
		new YamlConfigLoader("lang/ENU.yml", "lang/ENU.yml");

		loadAll();
	}

	public void loadAll()
	{

		EconomyLife.getInstance().getLogger().log(Level.INFO, "Load language files...");
		
		File folder = new File(EconomyLife.getInstance().getDataFolder(), "lang");
		File[] listOfFiles = folder.listFiles();

		for (File file : listOfFiles)
		{
			if (file.isFile() && file.getName().endsWith(".yml"))
			{

				EconomyLife.getInstance().getLogger().log(Level.INFO, file.getName());

				String code = file.getName().replace(".yml", "");
				languages.put(code, new Language(code));
			}
		}
	}
	
	public static void addDefault(String path, String value){
		LanguageManager languageManager = EconomyLife.getLanguageManager();
		for(Language language : languageManager.languages.values())
			language.addDefault(path, value);
	}

	public static String getString(EcoPlayer ecoPlayer, String path)
	{
		return getString(ecoPlayer.getLanguageCode(), path);
	}

	public static String getString(String code, String path)
	{
		LanguageManager languageManager = EconomyLife.getLanguageManager();
		if (languageManager.languages.containsKey(code))
		{
			Language language = languageManager.languages.get(code);
			String value = language.getString(path);

			return (value.equals("")) ? code + " " + path + " doesent exist!" : value;
		}

		return code + " language doesent exist!";
	}
}
