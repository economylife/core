/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.config;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;

import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.YamlConfiguration;

import eu.economylife.core.EconomyLife;

public class YamlConfigLoader
{

	private String path;
	private File file;

	private YamlConfiguration ymlConfig;

	public YamlConfigLoader(String path)
	{
		this(path, "");
	}

	public YamlConfigLoader(String path, String defaultConfigPath)
	{

		this.path = path;

		file = new File(EconomyLife.getInstance().getDataFolder(), path);

		if (!file.exists())
		{

			file.getParentFile().mkdirs();

			try
			{

				file.createNewFile();
				if (!defaultConfigPath.equals(""))
					getDefaultConfig(defaultConfigPath);

			} catch (IOException e)
			{

				e.printStackTrace();
			}
		}

		load();
	}

	public void load()
	{

		ymlConfig = new YamlConfiguration();
		try
		{

			ymlConfig.load(file);

		} catch (FileNotFoundException e)
		{

			e.printStackTrace();
		} catch (IOException e)
		{

			e.printStackTrace();
		} catch (InvalidConfigurationException e)
		{

			e.printStackTrace();
		}
	}

	public void getDefaultConfig(String path)
	{

		ymlConfig = new YamlConfiguration();
		try
		{
			InputStreamReader defConfigStream = new InputStreamReader(
					EconomyLife.getInstance().getResource("res/configs/" + path), "UTF8");
			if (defConfigStream != null)
			{
				ymlConfig = YamlConfiguration.loadConfiguration(defConfigStream);
			}

		} catch (IOException e)
		{

			e.printStackTrace();
		}

		save();
	}

	public void save()
	{

		try
		{

			file = new File(EconomyLife.getInstance().getDataFolder(), path);

			if (!file.exists())
			{
				file.getParentFile().mkdirs();

				try
				{
					file.createNewFile();
				} catch (IOException e)
				{
					e.printStackTrace();
				}
			}

			ymlConfig.save(file);

		} catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public void delete()
	{

		file = new File(EconomyLife.getInstance().getDataFolder(), path);

		if (file.exists())
		{
			file.delete();
		}
	}

	public YamlConfiguration getYml()
	{
		return ymlConfig;
	}
}
