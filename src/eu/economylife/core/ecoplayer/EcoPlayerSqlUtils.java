/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.ecoplayer;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.bukkit.configuration.file.YamlConfiguration;

import eu.economylife.core.EconomyLife;
import eu.economylife.core.config.YamlConfigLoader;
import eu.economylife.core.group.GroupManager;
import eu.economylife.core.sql.SqlManager;
import eu.economylife.core.utils.LocationUtils;

public interface EcoPlayerSqlUtils
{
	public static boolean exist(EcoPlayer ecoPlayer)
	{
		SqlManager sqlManager = EconomyLife.getSqlManager();
		try
		{
			PreparedStatement request = sqlManager.getConnection()
					.prepareStatement("SELECT UUID From `Player` WHERE UUID=?");
			request.setString(1, ecoPlayer.getShortUUID());
			ResultSet result = request.executeQuery();
			return result.first();
		} catch (SQLException e)
		{
			return false;
		}
	}

	public static void insert(EcoPlayer ecoPlayer)
	{
		SqlManager sqlManager = EconomyLife.getSqlManager();
		YamlConfigLoader configLoader = new YamlConfigLoader("Config.yml");
		YamlConfiguration config = configLoader.getYml();

		try
		{
			PreparedStatement insert = sqlManager.getConnection()
					.prepareStatement("INSERT INTO `Player` values(?,?,?,?,?,?,?,?,?);");
			insert.setString(1, ecoPlayer.getShortUUID());// UUID
			insert.setString(2, ecoPlayer.getName());// Name
			insert.setString(3, EconomyLife.getGroupManager().getDefaultGroup().getId());// Group
			insert.setInt(4, config.getInt("config.player.money.default", 0));// Money
			insert.setString(5, ecoPlayer.getPlayer().getAddress().getHostName());// IPAddress
			insert.setDate(6, new Date(Calendar.getInstance().getTimeInMillis()));// FirstLogin
			insert.setDate(7, new Date(Calendar.getInstance().getTimeInMillis()));// LastLogin
			insert.setInt(8, 0);// PlayTimeMin
			insert.setString(9, config.getString("config.player.location.spawn"));// LogoutPosition
			insert.executeUpdate();
			insert.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void load(EcoPlayer ecoPlayer)
	{
		SqlManager sqlManager = EconomyLife.getSqlManager();
		GroupManager groupManager = EconomyLife.getGroupManager();

		try
		{
			PreparedStatement request = sqlManager.getConnection()
					.prepareStatement("SELECT * From `Player` WHERE UUID=?");
			request.setString(1, ecoPlayer.getShortUUID());
			ResultSet result = request.executeQuery();
			if (result.first())
			{
				ecoPlayer.setGroup(groupManager.getGroupByID(result.getString(3)));
				ecoPlayer.setMoney(result.getFloat(4));
				ecoPlayer.setLogoutLocation(LocationUtils.toLoc(result.getString(9)));
			}

		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}

	public static void save(EcoPlayer ecoPlayer)
	{
		SqlManager sqlManager = EconomyLife.getSqlManager();

		try
		{
			PreparedStatement update = sqlManager.getConnection().prepareStatement(
					"UPDATE `Player` SET Name=?, GroupName=?, Money=?, IPAddress=?, PlayedMinutes=?, LogoutLocation=? WHERE UUID=?");
			update.setString(1, ecoPlayer.getName());// Name
			update.setString(2, ecoPlayer.getGroup().getId());// Group
			update.setFloat(3, ecoPlayer.getMoney());// Money
			update.setString(4, ecoPlayer.getPlayer().getAddress().getHostName());// IPAddress
			update.setInt(5, 0);// PlayTimeMin
			update.setString(6, LocationUtils.toString(ecoPlayer.getPlayer().getLocation()));// Logoutlocation
			update.setString(7, ecoPlayer.getShortUUID());// UUID
			update.executeUpdate();
			update.close();
		} catch (SQLException e)
		{
			e.printStackTrace();
		}
	}
}
