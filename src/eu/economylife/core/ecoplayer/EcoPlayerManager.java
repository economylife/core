/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.ecoplayer;

import java.util.HashMap;

import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import eu.economylife.core.EconomyLife;
import eu.economylife.core.config.YamlConfigLoader;
import eu.economylife.core.utils.LocationUtils;

public class EcoPlayerManager implements Listener
{

	private HashMap<Player, EcoPlayer> ecoPlayers;
	
	public EcoPlayerManager()
	{

		EconomyLife economyLife = EconomyLife.getInstance();
		economyLife.getServer().getPluginManager().registerEvents(this, economyLife);

		ecoPlayers = new HashMap<>();
		
		loadOnlinePlayer();
	}
    
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent e)
	{
		e.setJoinMessage("");
		loadPlayer(e.getPlayer());
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent e)
	{
		e.setQuitMessage("");
		removePlayer(e.getPlayer());
	}

	public void loadOnlinePlayer()
	{
		EconomyLife economyLife = EconomyLife.getInstance();

		for (Player player : economyLife.getServer().getOnlinePlayers())
			loadPlayer(player);
	}
	
	public void removeOnlinePlayer()
	{
		EconomyLife economyLife = EconomyLife.getInstance();

		for (Player player : economyLife.getServer().getOnlinePlayers())
			removePlayer(player);
	}

	public void loadPlayer(Player player)
	{
		YamlConfigLoader configLoader = new YamlConfigLoader("Config.yml");
		YamlConfiguration config = configLoader.getYml();
		Location lobbyLocation = LocationUtils.toLoc(config.getString("config.player.location.lobby"));
		player.teleport(lobbyLocation);
		
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				ecoPlayers.put(player, new EcoPlayer(player));
			}
		}).start();
	}

	public void removePlayer(Player player)
	{
		new Thread(new Runnable()
		{
			@Override
			public void run()
			{
				EcoPlayerSqlUtils.save(ecoPlayers.get(player));
				ecoPlayers.remove(player);
			}
		}).start();
	}
	
	public HashMap<Player, EcoPlayer> getEcoPlayers() {
		return ecoPlayers;
	}
	
	public static EcoPlayer getEcoPlayer(Player player) {
		return EconomyLife.getPlayerManager().getEcoPlayers().get(player);
	}
	
	public static EcoPlayer getEcoPlayer(String name) {
		for(Player player : EconomyLife.getPlayerManager().getEcoPlayers().keySet())
			if(player.getName().toLowerCase().startsWith(name.toLowerCase()))
				return getEcoPlayer(player);
		return null;
	}
}
