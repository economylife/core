/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.ecoplayer;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import eu.economylife.core.EconomyLife;
import eu.economylife.core.group.Group;
import eu.economylife.core.permissions.PermissionsManager;

public class EcoPlayer
{

	private Player player;
	private UUID uuid;
	private String name;

	private String languageCode = "ENU";
	private Group group;
	private float money;
	private Location logoutLocation;

	public EcoPlayer(String name)
	{
		EcoPlayerSqlUtils.load(this);
	}

	public EcoPlayer(UUID uuid)
	{
		EcoPlayerSqlUtils.load(this);
	}
	
	public EcoPlayer(Player player)
	{	
		this.player = player;
		this.uuid = player.getUniqueId();
		this.name = player.getName();

		if (!EcoPlayerSqlUtils.exist(this))
			EcoPlayerSqlUtils.insert(this);

		EcoPlayerSqlUtils.load(this);

		player.teleport(logoutLocation);
	}

	public boolean hasPermissions(String name)
	{
		int level = PermissionsManager.getPermissionLevel(name);
		
		//TODO ERROR
		if(level == -1){
			System.out.println("Permission Error: " + name + " doesnot exist!");
			return false;
		}
		
		return (group.getPermLevel() >= level);
	}

	public Player getPlayer()
	{
		return player;
	}

	public Location getLocation() 
	{
		return getPlayer().getLocation();
	}
	
	public void setPlayer(Player player)
	{
		this.player = player;
	}

	public UUID getUUID()
	{
		return uuid;
	}

	public String getShortUUID()
	{
		return uuid.toString().replace("-", "");
	}

	public void setUUID(UUID uuid)
	{
		this.uuid = uuid;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String name)
	{
		this.name = name;
	}

	public Group getGroup()
	{
		return group;
	}

	public void setGroup(Group group)
	{
		this.group = group;
	}

	public float getMoney()
	{
		return money;
	}

	public void setMoney(float money)
	{
		this.money = money;
	}
	
	public void addMoney(float amount)
	{
		this.money = money + amount;
	}

	public Location getLogoutLocation()
	{
		return logoutLocation;
	}

	public void setLogoutLocation(Location logoutLocation)
	{
		this.logoutLocation = logoutLocation;
	}

	public String getLanguageCode()
	{
		return languageCode;
	}

	public void setLanguageCode(String languageCode)
	{
		this.languageCode = languageCode;
	}
}
