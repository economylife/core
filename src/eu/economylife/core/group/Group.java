/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.group;

public class Group
{

	private String id;
	private String name;
	private String shortName;
	private String prefix;
	private int permLevel;
	private boolean defaultGroup;

	public Group(String id, String name, String shortName, String prefix, int permLevel, boolean defaultGroup)
	{
		this.id = id;
		this.name = name;
		this.shortName = shortName;
		this.prefix = prefix;
		this.permLevel = permLevel;
		this.defaultGroup = defaultGroup;
	}

	public String getName()
	{
		return name;
	}

	public String getShortName()
	{
		return shortName;
	}

	public String getPrefix()
	{
		return prefix;
	}


	public int getPermLevel()
	{
		return permLevel;
	}

	public String getId()
	{
		return id;
	}

	public boolean isDefaultGroup()
	{
		return defaultGroup;
	}
}
