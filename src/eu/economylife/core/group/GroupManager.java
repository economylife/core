/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.group;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import eu.economylife.core.EconomyLife;
import eu.economylife.core.config.YamlConfigLoader;

public class GroupManager
{
	private ArrayList<Group> groups;

	private Group defaultGroup;

	// Config
	private YamlConfigLoader configLoader;

	public GroupManager()
	{
		groups = new ArrayList<>();

		configLoader = new YamlConfigLoader("Groups.yml", "Groups.yml");

		YamlConfiguration config = configLoader.getYml();

		// Load Groups
		for (String key : config.getConfigurationSection("config.groups").getKeys(false))
		{
			String id = key;
			String fullName = config.getString("config.groups." + key + ".name.full", "Default");
			String shortName = config.getString("config.groups." + key + ".name.short", "D");
			String prefix = config.getString("config.groups." + key + ".prefix", "§f");
			int permissionLevel = config.getInt("config.groups." + key + ".permissionlevel", 0);
			boolean defaultGroup = config.getBoolean("config.groups." + key + ".default", false);

			addGroup(new Group(id, fullName, shortName, prefix, permissionLevel, defaultGroup));
		}
	}

	private void addGroup(Group group)
	{
		groups.add(group);
	}
	
	public static Group getGroupByName(String name)
	{
		GroupManager groupManager = EconomyLife.getGroupManager();
		for(Group tempGroup : groupManager.getGroups())
			if(tempGroup.getName().toLowerCase().startsWith(name.toLowerCase()))
				return tempGroup;
		return null;
	}
	
	public static Group getGroupByID(String id)
	{
		GroupManager groupManager = EconomyLife.getGroupManager();
		for(Group tempGroup : groupManager.getGroups())
			if(tempGroup.getId().equals(id))
				return tempGroup;
		return getDefaultGroup();
	}
	
	public static Group getDefaultGroup()
	{
		GroupManager groupManager = EconomyLife.getGroupManager();
		for(Group tempGroup : groupManager.getGroups())
			if(tempGroup.isDefaultGroup())
				return tempGroup;
		return null;
	}
	
	public ArrayList<Group> getGroups()
	{
		return groups;
	}
}
