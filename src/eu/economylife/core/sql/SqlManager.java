package eu.economylife.core.sql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import eu.economylife.core.config.YamlConfigLoader;

public class SqlManager {

    private Connection connection;

    private String url;
    private int port;
    private String database;
    private String user;
    private String password;

    public SqlManager() {
        readSettings();
        openConnection();
    }

    @Override
    protected void finalize() throws Throwable {
        closeConnection();
        super.finalize();
    }

    private void readSettings() {
    	
        YamlConfigLoader configLoader = new YamlConfigLoader("Config.yml", "Config.yml");
        YamlConfiguration config = configLoader.getYml();
        url = config.getString("config.server.sql.url", "localhost");
        port = config.getInt("config.server.sql.port", 3306);
        database = config.getString("config.server.sql.database", "economylife");
        user = config.getString("config.server.sql.user", "root");
        password = config.getString("config.server.sql.password", "");
    }
  

    private void openConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String complett = "jdbc:mysql://" + url + ":" + port + "/" + database;
            Bukkit.getLogger().info("[EconomyLife] Connecting to:" + complett);
            connection = DriverManager.getConnection(complett, user, password);
        } catch (Exception e) {
            Bukkit.getLogger().info("[EconomyLife] Error Connecting to SQL:" + e.toString());
        }
    }

    private void closeConnection() {
        try {
            connection.close();
        } catch (Exception e) {
            Bukkit.getLogger().info("[Aldorin] Error Closing Connection");
        }
    }

    public Connection getConnection() {
        try {
            if (connection != null && !connection.isClosed()) {
                return connection;
            }
            Bukkit.getLogger().info("[Tenyrian]Error Connection not open");
            if (connection == null) {
                Bukkit.getLogger().info("[Aldorin] Null!");
            } else {
                Bukkit.getLogger().info("[Aldorin] Closed!");
            }
        } catch (SQLException ex) {
        }
        openConnection();
        return connection;
    }
}