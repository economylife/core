/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.chat;

import org.bukkit.entity.Player;

import eu.economylife.core.EconomyLife;
import eu.economylife.core.ecoplayer.EcoPlayer;
import eu.economylife.core.ecoplayer.EcoPlayerManager;
import eu.economylife.core.language.LanguageManager;

public interface Chat
{

	public static void sendMessage(Player player, MessageType messageType, String path)
	{
		sendMessage(player, messageType, "Server", path, new ChatArg[] {});
	}

	public static void sendMessage(Player player, MessageType messageType, String name, String path)
	{
		sendMessage(player, messageType, name, path, new ChatArg[] {});
	}

	public static void sendMessage(Player player, MessageType messageType, String path, ChatArg... args)
	{
		sendMessage(player, messageType, "Server", path, args);
	}

	public static void sendMessage(Player player, MessageType messageType, String name, String path, ChatArg... args)
	{
		EcoPlayer ecoPlayer = EcoPlayerManager.getEcoPlayer(player);
		
		sendMessage(ecoPlayer, messageType, name, path, args);
	}
	
	public static void sendMessage(EcoPlayer ecoPlayer, MessageType messageType, String path)
	{
		sendMessage(ecoPlayer, messageType, "Server", path, new ChatArg[] {});
	}

	public static void sendMessage(EcoPlayer ecoPlayer, MessageType messageType, String name, String path)
	{
		sendMessage(ecoPlayer, messageType, name, path, new ChatArg[] {});
	}

	public static void sendMessage(EcoPlayer ecoPlayer, MessageType messageType, String path, ChatArg... args)
	{
		sendMessage(ecoPlayer, messageType, "Server", path, args);
	}
	
	public static void sendMessage(EcoPlayer ecoPlayer, MessageType messageType, String name, String path, ChatArg... args)
	{
		String nameColorTag = "";
		String argColorTag = "";
		String msgColorTag = "";

		switch (messageType)
		{
		case NONE:
			argColorTag = "§f";
			msgColorTag = "§f";
			break;
		case NORMAL:
			nameColorTag = "§6";
			argColorTag = "§f";
			msgColorTag = "§7";
			break;
		case ERROR:
			nameColorTag = "§c";
			argColorTag = "§c";
			msgColorTag = "§7";
			break;
		default:
			argColorTag = "§f";
			argColorTag = "§f";
			break;
		}

		String message = LanguageManager.getString(ecoPlayer, path);

		// Message Formatting
		message = replaceArgs(message, args, argColorTag, msgColorTag);

		// Name Formatting
		name = (name.equals("")) ? msgColorTag : "§7[" + nameColorTag + name + "§7] " + msgColorTag;

		ecoPlayer.getPlayer().sendMessage(name + message);
	}

	static String replaceArgs(String message, ChatArg[] args, String argColorTag, String msgColorTag)
	{
		for (ChatArg arg : args)
		{
			message = message.replace(arg.getReplace(), argColorTag + arg.getValue() + msgColorTag);
		}

		return message;
	}
}
