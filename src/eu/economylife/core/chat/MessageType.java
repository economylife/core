/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core.chat;

public enum MessageType
{
	NONE,
	NORMAL,
	ERROR;
}
