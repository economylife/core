/** Copyright economylife.core
All rights reserved.
*/
package eu.economylife.core;

import org.bukkit.plugin.java.JavaPlugin;

import eu.economylife.core.command.CommandManager;
import eu.economylife.core.command.CommandRegister;
import eu.economylife.core.config.ConfigManager;
import eu.economylife.core.ecoplayer.EcoPlayerManager;
import eu.economylife.core.group.GroupManager;
import eu.economylife.core.language.LanguageManager;
import eu.economylife.core.permissions.PermissionsManager;
import eu.economylife.core.sql.SqlManager;

public class EconomyLife extends JavaPlugin
{

	private static EconomyLife instance;

	// Manager
	private SqlManager sqlManager;
	private ConfigManager configManager;
	private LanguageManager languageManager;
	private PermissionsManager permissionsManager;
	private GroupManager groupManager;
	private CommandManager commandManager;
	private EcoPlayerManager playerManager;

	@Override
	public void onEnable()
	{
		instance = this;

		// Manager Laden
		loadManager();
	}

	@Override
	public void onDisable()
	{
		playerManager.removeOnlinePlayer();
	}

	private void loadManager()
	{
		sqlManager = new SqlManager();
		configManager = new ConfigManager();
		languageManager = new LanguageManager();
		permissionsManager = new PermissionsManager();
		groupManager = new GroupManager();
		commandManager = new CommandManager();
		playerManager = new EcoPlayerManager();
		
		// load Commands
		new CommandRegister();
	}

	public static EconomyLife getInstance()
	{
		return instance;
	}
	
	public static SqlManager getSqlManager(){
		return getInstance().sqlManager;
	}
	
	public static PermissionsManager getPermissionsManager(){
		return getInstance().permissionsManager;
	}
	
	public static GroupManager getGroupManager(){
		return getInstance().groupManager;
	}
	
	public static CommandManager getCommandManager(){
		return getInstance().commandManager;
	}

	public static LanguageManager getLanguageManager()
	{
		return getInstance().languageManager;
	}

	public static EcoPlayerManager getPlayerManager()
	{
		return getInstance().playerManager;
	}
}
