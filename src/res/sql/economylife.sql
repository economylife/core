-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 27. Sep 2017 um 20:07
-- Server-Version: 10.1.22-MariaDB
-- PHP-Version: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `economylife`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bans`
--
-- Erstellt am: 24. Sep 2017 um 11:05
--

CREATE TABLE `bans` (
  `ID` int(11) NOT NULL,
  `UUID` varchar(32) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  `From` datetime NOT NULL,
  `To` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONEN DER TABELLE `bans`:
--   `UUID`
--       `player` -> `UUID`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bansdescription`
--
-- Erstellt am: 24. Sep 2017 um 11:05
--

CREATE TABLE `bansdescription` (
  `BanID` int(11) NOT NULL,
  `FromUUID` varchar(32) NOT NULL,
  `Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONEN DER TABELLE `bansdescription`:
--   `FromUUID`
--       `player` -> `UUID`
--   `BanID`
--       `bans` -> `ID`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bansproof`
--
-- Erstellt am: 24. Sep 2017 um 11:05
--

CREATE TABLE `bansproof` (
  `ID` int(11) NOT NULL,
  `BanID` int(11) NOT NULL,
  `FromUUID` varchar(32) NOT NULL,
  `DateTime` datetime NOT NULL,
  `Proof` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONEN DER TABELLE `bansproof`:
--   `FromUUID`
--       `player` -> `UUID`
--   `BanID`
--       `bans` -> `ID`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `player`
--
-- Erstellt am: 24. Sep 2017 um 11:05
--

CREATE TABLE `player` (
  `UUID` varchar(32) NOT NULL,
  `Name` text NOT NULL,
  `Group` text NOT NULL,
  `Money` float NOT NULL,
  `IPAddress` text NOT NULL,
  `FirstLogin` datetime NOT NULL,
  `LastLogin` datetime NOT NULL,
  `PlayedMinutes` int(11) NOT NULL,
  `LogoutLocation` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONEN DER TABELLE `player`:
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `warns`
--
-- Erstellt am: 24. Sep 2017 um 11:05
--

CREATE TABLE `warns` (
  `ID` int(11) NOT NULL,
  `UUID` varchar(32) NOT NULL,
  `FromUUID` varchar(32) NOT NULL,
  `Active` tinyint(1) NOT NULL,
  `DateTime` datetime NOT NULL,
  `Points` int(11) NOT NULL,
  `Description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONEN DER TABELLE `warns`:
--   `FromUUID`
--       `player` -> `UUID`
--   `UUID`
--       `player` -> `UUID`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `warnsproof`
--
-- Erstellt am: 27. Sep 2017 um 18:06
--

CREATE TABLE `warnsproof` (
  `ID` int(11) NOT NULL,
  `WarnProofID` int(11) NOT NULL,
  `FromUUID` varchar(32) NOT NULL,
  `DateTime` datetime NOT NULL,
  `Proof` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONEN DER TABELLE `warnsproof`:
--   `FromUUID`
--       `player` -> `UUID`
--   `WarnProofID`
--       `warns` -> `ID`
--

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `bans`
--
ALTER TABLE `bans`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `UUID` (`UUID`);

--
-- Indizes für die Tabelle `bansdescription`
--
ALTER TABLE `bansdescription`
  ADD PRIMARY KEY (`BanID`),
  ADD KEY `FromUUID` (`FromUUID`);

--
-- Indizes für die Tabelle `bansproof`
--
ALTER TABLE `bansproof`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FromUUID` (`FromUUID`),
  ADD KEY `BanID` (`BanID`);

--
-- Indizes für die Tabelle `player`
--
ALTER TABLE `player`
  ADD PRIMARY KEY (`UUID`),
  ADD UNIQUE KEY `UUID` (`UUID`);

--
-- Indizes für die Tabelle `warns`
--
ALTER TABLE `warns`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FromUUID` (`FromUUID`),
  ADD KEY `UUID` (`UUID`);

--
-- Indizes für die Tabelle `warnsproof`
--
ALTER TABLE `warnsproof`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `FromUUID` (`FromUUID`),
  ADD KEY `WarnProof` (`WarnProofID`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `bans`
--
ALTER TABLE `bans`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `bansproof`
--
ALTER TABLE `bansproof`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `warns`
--
ALTER TABLE `warns`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT für Tabelle `warnsproof`
--
ALTER TABLE `warnsproof`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `bans`
--
ALTER TABLE `bans`
  ADD CONSTRAINT `bans_ibfk_1` FOREIGN KEY (`UUID`) REFERENCES `player` (`UUID`);

--
-- Constraints der Tabelle `bansdescription`
--
ALTER TABLE `bansdescription`
  ADD CONSTRAINT `bansdescription_ibfk_1` FOREIGN KEY (`FromUUID`) REFERENCES `player` (`UUID`),
  ADD CONSTRAINT `bansdescription_ibfk_2` FOREIGN KEY (`BanID`) REFERENCES `bans` (`ID`);

--
-- Constraints der Tabelle `bansproof`
--
ALTER TABLE `bansproof`
  ADD CONSTRAINT `bansproof_ibfk_1` FOREIGN KEY (`FromUUID`) REFERENCES `player` (`UUID`),
  ADD CONSTRAINT `bansproof_ibfk_2` FOREIGN KEY (`BanID`) REFERENCES `bans` (`ID`);

--
-- Constraints der Tabelle `warns`
--
ALTER TABLE `warns`
  ADD CONSTRAINT `warns_ibfk_1` FOREIGN KEY (`FromUUID`) REFERENCES `player` (`UUID`),
  ADD CONSTRAINT `warns_ibfk_2` FOREIGN KEY (`UUID`) REFERENCES `player` (`UUID`);

--
-- Constraints der Tabelle `warnsproof`
--
ALTER TABLE `warnsproof`
  ADD CONSTRAINT `warnsproof_ibfk_1` FOREIGN KEY (`FromUUID`) REFERENCES `player` (`UUID`),
  ADD CONSTRAINT `warnsproof_ibfk_2` FOREIGN KEY (`WarnProofID`) REFERENCES `warns` (`ID`);


--
-- Metadaten
--
USE `phpmyadmin`;

--
-- Metadaten für Tabelle bans
--

--
-- Metadaten für Tabelle bansdescription
--

--
-- Metadaten für Tabelle bansproof
--

--
-- Metadaten für Tabelle player
--

--
-- Metadaten für Tabelle warns
--

--
-- Metadaten für Tabelle warnsproof
--

--
-- Metadaten für Datenbank economylife
--

--
-- Daten für Tabelle `pma__pdf_pages`
--

INSERT INTO `pma__pdf_pages` (`db_name`, `page_descr`) VALUES
('economylife', 'save');

SET @LAST_PAGE = LAST_INSERT_ID();

--
-- Daten für Tabelle `pma__table_coords`
--

INSERT INTO `pma__table_coords` (`db_name`, `table_name`, `pdf_page_number`, `x`, `y`) VALUES
('economylife', 'bans', @LAST_PAGE, 665, 200),
('economylife', 'bansdescription', @LAST_PAGE, 665, 323),
('economylife', 'bansproof', @LAST_PAGE, 665, 405),
('economylife', 'player', @LAST_PAGE, 47, 79),
('economylife', 'warns', @LAST_PAGE, 663, 525),
('economylife', 'warnsproof', @LAST_PAGE, 659, 678);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
